﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_CrossPostBack
{
    public partial class WebPage2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Bad Practice

            //TextBox textNameObj = PreviousPage.FindControl("txtName") as TextBox;

            //lblName.Text = textNameObj.Text;

            //TextBox textNameObj1 = PreviousPage.FindControl("txtText") as TextBox;

            //lblName1.Text = textNameObj1.Text;

            #endregion

            #region Average Pracice

            //string strName = ((TextBox)PreviousPage.FindControl("txtName")).Text;

            //lblName.Text = strName;

            //string str = ((TextBox)PreviousPage.FindControl("txtName1")).Text;

            //lblName1.Text = str;

            #endregion

            #region Good Practice

            lblName.Text = ((TextBox)PreviousPage.FindControl("txtName")).Text;

            lblName1.Text = ((TextBox)PreviousPage.FindControl("txtName1")).Text;

            lblName2.Text = ((TextBox)PreviousPage.FindControl("txtName2")).Text;

            lblName3.Text = ((TextBox)PreviousPage.FindControl("txtName3")).Text;

            lblName4.Text = ((TextBox)PreviousPage.FindControl("txtName4")).Text;

            #endregion
        }
    }
}